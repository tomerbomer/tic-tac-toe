import random

def printBoard(board):
    """ Prints the board to screen. """
    for row in board:
        print(row)

def getRowCol(str_row_col):
    """ Gets a number from user and checks if it's in range (0 - 2).
    Returns a valid input.
    str_row_col = "row" or "column". """
    while True:
        num = input(f"Enter {str_row_col}: ")
        if num.isdigit():
            num = int(num)
            if num in range(0, 3):
                return num
        if num == "?":
            return "?"
        print(f"Invalid {str_row_col} num. Must be between 0 and 2.")

def getSign(player):
    """ Returns the sign of the player: 'X' for 1, 'O' for 2. """
    return "X" if player == 1 else "O"

def setCell(board, row, col, sign):
    """ Checks if the cell is empty. if so - sets the correct sign to the chosen cell.
    Returns True if cell was empty. False otherwise."""
    if board[row][col] == "_":
        board[row][col] = sign
        return True
    else:
        return False

def checkWin(board, sign):
    """ Checks if the player won. Returns True/False accordingly. """
    for i in range(0, 3):
        if board[i].count(sign) == 3:
            return True
        if board[0][i] == sign and board[1][i] == sign and board[2][i] == sign:
            return True
    if board[1][1] == sign:
        if board[0][0] == sign and board[2][2] == sign:
            return True
        elif board[0][2] == sign and board[2][0] == sign:
            return True
    else:
        return False

def checkEmpty(board):
    """ Checks if there are empty cells in the board.
    Returns True/False accordingly. """
    emptyCells = 0
    for i in range(0, 3):
        emptyCells += board[i].count("_")
    return True if emptyCells == 0 else False

def checkForBoard(board, sign):
    """ Checks if there are 2 cells with given sign
        and an empty cell for each list in given board.
    Returns a list of the empty cell indexes if True; None otherwise. """
    for i in range(0, 3):
        if board[i].count(sign) == 2 and board[i].count("_") == 1:
            return [i, board[i].index("_")]
    return None

def checkVictoryMove(board, sign):
    """ Checks if player can win this turn.
    Returns the victory cell (=string), or None if player can't win this turn. """
    win = checkForBoard(board, sign)
    if win != None:
        return f"({win[0]}, {win[1]})"

    transpose_board = [[board[row][0] for row in range(0, 3)],
                       [board[row][1] for row in range(0, 3)],
                       [board[row][2] for row in range(0, 3)]]
    win = checkForBoard(transpose_board, sign)
    if win != None:
        return f"({win[1]}, {win[0]})"

    diagonals = [[board[i][i] for i in range(0, 3)],
                 [board[0][2], board[1][1], board[2][0]]]
    win = checkForBoard(diagonals, sign)
    if win != None:
        if win[0] == 0 or win[1] == 1:
            return f"({win[1]}, {win[1]})"
        else:
            win[0] = 2 if win[1] == 0 else 0
            return f"({win[1]}, {win[0]})"
    else:
        return None



def runGame(comp):
    """ Runs the Tic Tac Toe game """
    board = [["_", "_", "_"], ["_", "_", "_"], ["_", "_", "_"]]
    player = 1
    print("Game started: Player1 = X, Player2 = O.")
    while True:
        printBoard(board)
        print(f"Player{player}'s Turn:")
        while True:
            if player == comp or comp == 3:
                chosenRow = random.randint(0, 2)
                chosenCol = random.randint(0, 2)
            else:
                chosenRow = getRowCol("row")
                chosenCol = getRowCol("column")
                """ if player entered '?' for row and col
                he wants to check if he can win this turn, and how. """
                if chosenRow == "?" and chosenCol == "?":
                    victoryMove = checkVictoryMove(board, getSign(player))
                    victoryMove = f"cell {victoryMove} for the win." if victoryMove != None else "No victory this turn."
                    print(victoryMove)
                    continue
            if setCell(board, chosenRow, chosenCol, getSign(player)):
                break
            elif player != comp and comp != 3:
                print("The chosen spot is taken. choose an empty spot.")
        if checkWin(board, getSign(player)):
            printBoard(board)
            print(f"Player {player} wins!")
            break
        elif checkEmpty(board):
            printBoard(board)
            print("No more valid moves... It's a tie!")
            break
        player = 1 if player == 2 else 2


def main():
    while True:
        print("Tic Tac Toe")
        print("Would you like to play against the computer?")
        comp = int(input("0=no; 1=player1 computer; 2=player2 computer; 3=both computer: "))
        runGame(comp)
        if input("Play again? ('n'= quit): ") == "n":
            break


main()